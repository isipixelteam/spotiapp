import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sinfoto'
})
export class SinfotoPipe implements PipeTransform {

  transform( images: any[], size: number): any {
    let noimage = 'assets/img/noimage.png';
    
    if( !images ){
      return noimage;
    }

    return ( images.length ) ? images[size-1].url : noimage;
  }

}