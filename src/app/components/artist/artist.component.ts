import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent implements OnInit {

  artista : any = {};
  toptracks : any = {};

  constructor(
    private activatedRoute: ActivatedRoute,
    public _spotifyService: SpotifyService ){

  }

  ngOnInit() {

    this.activatedRoute.params
      .map( (params) => params['id'])
      .subscribe( (id) => {
        this._spotifyService.getArtista( id )
          .subscribe( artista => {
            console.log(artista);
            this.artista = artista;
        });

        this._spotifyService.getTop( id )
          .map( (resp: any) => resp.tracks)
          .subscribe( toptracks => {
            console.log(toptracks);
            this.toptracks = toptracks;
        });
      });

  }

  verSpotify(){
    
  }

}
