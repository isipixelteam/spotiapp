import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SpotifyService {

  artistas  : any[]  = [];
  urlSpotify: string = 'https://api.spotify.com/v1/';
  token     : string = 'BQDpOr_tfQTULMwOfVlbtQnSYboUFasCSfgFkpada-jhY1dOln0UI5jnA4obIl5ok42fpX5_rMG6exjmEis';
  country   : string = 'US';

  constructor( public httpClient: HttpClient ) { 
    console.log('Servicio spotify listo.');
  }

  private getHeaders(): HttpHeaders{
    let headers = new HttpHeaders({
      'authorization': `Bearer ${ this.token }`
    });

    return headers;
  }

  getTop( id: string ){
    let url = `${ this.urlSpotify }artists/${ id }/top-tracks?country=${ this.country }`;
    let headers = this.getHeaders();

    return this.httpClient.get(url, { headers })
      .map( (resp: any) => {
        return resp;
    })
  }

  getArtista( id: string ){
    let url = `${ this.urlSpotify }artists/${ id }`;
    let headers = this.getHeaders();

    return this.httpClient.get(url, { headers })
      .map( (resp: any) => {
        return resp;
    })
  }

  getArtistas( termino: string ){

    let url = `${ this.urlSpotify }search?query=${ termino }&type=artist&offset=0&limit=20`;
    let headers = this.getHeaders();

    return this.httpClient.get(url, { headers })
      .map( (resp: any) => {
        this.artistas = resp.artists.items;
        return this.artistas;
    })

  }
}
